const config = {
    apiDomain: "http://localhost:8080a",
    clientDomain: "http://localhost:3000",
    accessTokenKey: "testMe",
    googleApiKey: "GOOGLE_API_KEY",
    mediaFileDomain: "http://localhost:8080", //If you allow images to be uploaded to your local server
    saveMediaFileLocal: false, //Set this to true if you allow images to be uploaded to your local server
    sendgridApiKey: "SENDGRID_API_KEY",
    sendgridDailyLimit: "SENDGRID_DAILY_LIMIT_FOR_FREETIER",
    elasticemailApiKey: "ELASTICEMAIL_API_KEY",
    elasticemailDailyLimit: "ELASTICEMAIL_DAILY_LIMIT_FOR_FREETIER",
    passwordCallbackUrl: "https://www.example.com",
    senderEmail: "SYSTEM_EMAIL_SENDER_EMAIL"
};

export default config;
