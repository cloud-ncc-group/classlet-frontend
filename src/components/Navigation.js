import React, {useState, useEffect} from 'react';
import axios from 'axios';
import jwt from 'jsonwebtoken';
import config from '../config';
import {Button, Icon, Label, Menu} from "semantic-ui-react";
import {Link} from "react-router-dom";
import Login from "../pages/authentication/login/Login";


const Navigation = props => {

        const {auth, setAuth} = props;

        async function checkSession() {
            try {
                await axios({
                    method: 'get',
                    url: `${config.apiDomain}/session`,
                    headers: {
                        authorization: localStorage.getItem(config.accessTokenKey),
                    }
                });
            } catch (e) {
                if (e.response.status === 401) {
                    localStorage.clear();
                    window.location.reload();
                }
            }
        };


        return (
            <div className={"normalMenu"}>
                <Menu widths={3}>
                    <Menu.Item><h1>Classlet</h1></Menu.Item>
                    <Menu.Item><h2>CNG 495 - Assignment 1</h2></Menu.Item>
                    {!auth ? <Menu.Item><Login setAuth={setAuth}/></Menu.Item>
                        : <Menu.Item><Button onClick={() => {
                            {
                                setAuth(false);
                            }
                        }}>
                            <Icon name="sign out"/>
                            Sign Out</Button></Menu.Item>}
                </Menu>
            </div>

        );
    }
;

export default Navigation;
