import React, {useState} from 'react';
import LoginForm from './LoginForm';
import {Button, Icon, Modal} from "semantic-ui-react";

const Login = (props) => {

    const {setAuth} = props;

    const [open, setOpen] = useState(false);

    return (
        <>
            <Button color="blue" onClick={() => setOpen(true)}>
                <h3><Icon name="sign in"/>Login</h3></Button>
            <Modal open={open} closeIcon size="tiny" onClose={() => setOpen(false)}>
                <Modal.Header>Login</Modal.Header>
                <Modal.Content>
                    <LoginForm setOpen={setOpen} setInnerAuth={setAuth}/>
                </Modal.Content>
            </Modal>
        </>
    );
}

export default Login;
