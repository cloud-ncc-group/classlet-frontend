import React, {useState, useEffect, useRef} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';
import config from '../../config';
import FileTable from "./FileTable";
import {Button, Card, Grid, Header, Icon, Input, Message, Segment} from "semantic-ui-react";
import Loading from "../../components/Loading";

const Browse = props => {


    const fileInputRef = useRef("fileInput");

    // initial is null.
    const [files, setFiles] = useState(null);

    const [file, setFile] = useState(null);

    // initial is false.
    const [error, setError] = useState(false);

    const [success, setSuccess] = useState(false);

    const [uploading, setUploading] = useState(false);

    const {auth, setAuth} = props;

    async function fetchFiles() {
        try {

            console.count("what");

            const res = await axios({
                method: 'get',
                url: `http://classlet.cloud:1234/list-all`,
            });
            const data = res.data;
            setFiles(data);
        } catch (e) {
            setError(true);
            setTimeout(() => {
                setError(false);
            }, 1000);
        }
    }

    useEffect(() => {
        fetchFiles();
    }, []);

    async function uploadFile() {
        try {

            setUploading(true);

            const formData = new FormData();
            formData.append('file', file);

            const res = await axios({
                method: 'post',
                url: 'http://classlet.cloud:1234/upload',
                data: formData,
                config: {headers: {'Content-Type': 'multipart/form-data'}}
            });

            setUploading(false);

            setSuccess(true);

            await fetchFiles();

            setTimeout(() => {
                setSuccess(false);
            }, 1500);
        } catch (e) {
            setError(true);
            setTimeout(() => {
                setError(false);
            }, 1000);
        }
    }

    return (
        (auth ? <Grid columns={2}>
                    <Grid.Column width={3}>
                        <Card>
                            <Card.Content>
                                <Segment placeholder>
                                    <Header icon>
                                        <Icon name='pdf file outline'/>
                                    </Header>
                                    <Button
                                        content="Choose File"
                                        primary
                                        onClick={() => fileInputRef.current.click()}
                                    />
                                    <input
                                        ref={fileInputRef}
                                        type="file"
                                        hidden
                                        onChange={(e) => {
                                            setFile(e.target.files[0])
                                        }}
                                    />
                                </Segment>
                                {file && <Message>
                                    <Message.Header>
                                        {file.name}
                                    </Message.Header>
                                </Message>}
                                {uploading && <Message icon>
                                    <Icon name='circle notched' loading/>
                                    <Message.Content>
                                        <Message.Header>
                                            Uploading...
                                        </Message.Header>
                                    </Message.Content>
                                </Message>}
                            </Card.Content>
                            < Card.Content extra>
                                {success && <Message positive icon>
                                    <Icon name="check"/>
                                    <Message.Content>
                                        <Message.Header>
                                            Uploaded!
                                        </Message.Header>
                                    </Message.Content></Message>}
                                {error && <Message negative icon>
                                    <Icon name="warning"/>
                                    <Message.Content>
                                    <Message.Header>
                                        Error.
                                    </Message.Header>
                                </Message.Content></Message>}
                                <Button fluid positive onClick={uploadFile}>Upload File</Button>
                            </Card.Content>
                        </Card>
                    </Grid.Column>
                    <Grid.Column width={13}>
                        {files && files.length > 0 ?
                            <FileTable setFiles={setFiles} files={files}/>
                            : files && files.length === 0 ?
                                <Message><Message.Header>Storage is empty.</Message.Header>
                                    <Message.Content>Please upload file.</Message.Content></Message>
                                : <Loading size={100}/>
                        }
                    </Grid.Column>
                </Grid>
                : <Grid>
                    <Grid.Column>
                        <Segment placeholder>
                            <Header icon>
                                <Icon name='lock'/>
                            </Header>
                        </Segment>
                    </Grid.Column>
                </Grid>
        )
    );
};


export default withRouter(Browse);
