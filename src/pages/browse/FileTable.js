import React, {useState} from "react";
import {Button, Icon, Message, Segment, Table} from "semantic-ui-react";
import axios from "axios";
import {Link} from "react-router-dom";

function FileTable(props) {

    const {files,setFiles} = props;

    const [success, setSuccess] = useState(false);
    const [error, setError] = useState(false);

    async function deleteFile(name) {
        try {
            const res = await axios({
                method: 'delete',
                url: `http://classlet.cloud:1234/delete?file=${name}`
            });

            const data = res.data;

            setSuccess(true);

            setTimeout(() => {
                fetchFiles();
            }, 1500);

            setTimeout(() => {
                setSuccess(false);
            }, 1500);
        } catch (e) {
            setError(true);
            setTimeout(() => {
                setError(false);
            }, 1000);
        }
    }

    async function fetchFiles() {
        try {

            console.count("what");

            const res = await axios({
                method: 'get',
                url: `http://classlet.cloud:1234/list-all`,
            });
            const data = res.data;
            setFiles(data);
        } catch (e) {
            setError(true);
            setTimeout(() => {
                setError(false);
            }, 1000);
        }
    }

    return (<div>
            {success && <Segment><Message positive icon>
                <Icon name="check"/>
                <Message.Content>
                <Message.Header>
                    File Deleted!
                </Message.Header>
            </Message.Content></Message></Segment>}
            {error && <Message negative icon>
                <Icon name="warning"/>
                <Message.Content>
                    <Message.Header>
                        Error.
                    </Message.Header>
                </Message.Content></Message>}
            <Table celled compact>
                <Table.Header fullWidth>
                    <Table.Row>
                        <Table.HeaderCell>File Name</Table.HeaderCell>
                        <Table.HeaderCell>Operation</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {files && files.map((file, i) => (
                        <Table.Row key={i}>
                            <Table.Cell>{file}</Table.Cell>
                            <Table.Cell collapsing>
                                <Button color="blue" icon="download" labelPosition="right" content="Download"
                                        onClick={() => {window.location.assign(`http://classlet.cloud:1234/download?file=${file}`);}}/>
                                <Button color="red" icon="delete" labelPosition="right" content="Delete"
                                        onClick={() => deleteFile(file)}/>
                            </Table.Cell>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        </div>

    );
}

export default FileTable;
