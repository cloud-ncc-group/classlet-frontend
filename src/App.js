import React, {useState} from 'react';
import {BrowserRouter as Router, Redirect, Route} from 'react-router-dom';
import Navigation from './components/Navigation';
import {Container} from "semantic-ui-react";
import Browse from "./pages/browse/Browse";


const App = () => {

    const routes = [
        {
            path: '/',
            exact: true,
            component: () => <Browse auth={auth} setAuth={setAuth}/>
        },
    ];

    const [auth, setAuth] = useState(true);

    return (
        <Router>
            <Container className="stickyContainer">
                <Navigation auth={auth} setAuth={setAuth}/>
                {routes.map((route, index) => (
                    <Route
                        key={index}
                        path={route.path}
                        exact={route.exact}
                        component={route.component}
                    />
                ))}
            </Container>
        </Router>);
};

export default App;
